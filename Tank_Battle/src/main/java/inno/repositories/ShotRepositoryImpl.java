package inno.repositories;

import inno.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotRepositoryImpl implements ShotRepository {

//    language=sql
    private static final String SQL_INSERT = "insert" +
        " into shot(game_id, shooter, target, time) " +
            "values (?, ?, ?, ?); ";
    private DataSource dataSource;

    public ShotRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot entity) {
        try (Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)){
            preparedStatement.setLong(1, entity.getGame().getID());
            preparedStatement.setLong(2, entity.getShooter().getPlayerID());
            preparedStatement.setLong(3,entity.getTarget().getPlayerID());
            preparedStatement.setString(4,entity.getShotTime());


            int insertedRowsCount = preparedStatement.executeUpdate();
            if (insertedRowsCount == 0){
                System.out.println("Cannot save entity");
            }
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()){
                long generatedId = resultSet.getLong("shot_id");
                entity.setShotId(generatedId);
            }

            resultSet.close();

        }
        catch (SQLException e){
            throw new IllegalStateException(e);
        }


    }
}
