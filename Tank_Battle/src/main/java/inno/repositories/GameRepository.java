package inno.repositories;

import inno.models.Game;
import inno.models.Player;

public interface GameRepository extends CRUDRepository<Game, Long> {
    @Override
    void save(Game entity);
    Game findByID(Long id);
    void addPlayersToGame(Long gameId, Player firstPlayer, Player secondPlayer);
    void update(Game game);
}
