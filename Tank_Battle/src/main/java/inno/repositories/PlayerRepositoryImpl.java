package inno.repositories;

import com.zaxxer.hikari.HikariConfig;
import inno.models.Player;

import javax.sql.DataSource;
import java.sql.*;

public class PlayerRepositoryImpl implements PlayerRepository {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/project_DB";
    private static final String DB_USERNAME = "postgres";
    private static final String DB_PASSWORD = "qwerty123";
    //language=sql
    private static final String SQL_INSERT = "insert into player (player_Name, player_IP, max_Score, wins_Quantity, losses_Quantity)\n" +
            "values (?, ?, ?, ?, ?); ";
    //language=sql
    private static final String SQL_UPDATE = "update player set " +
            "player_Name = ?, " + "player_IP = ?, " + "max_Score = ?, " +
            "wins_Quantity = ?, " + "losses_Quantity = ? where player_id = ?;";
    //language=sql
    private static final String SQL_FIND_BY_ID = "select * from player where player_id = ?;";


    //language=sql
    private static final String SQL_FIND_BY_NICKNAME = "select * from player where player_Name = ?";
    private final DataSource dataSource ;

    public PlayerRepositoryImpl(DataSource dataSource) {

        HikariConfig configuration;
        this.dataSource = dataSource;

    }


    Connection connection = null;
    Statement statement = null;
//    ResultSet resultSet = null;

    private RowMapper<Player> playerRawMap = row -> Player.builder()
            .playerID(row.getLong("player_id"))
            .playerName(row.getString("player_name"))
            .playerIP(row.getString("player_ip"))
            .playerScore(row.getInt("max_score"))
            .playerWins(row.getInt("wins_quantity"))
            .playerLosses(row.getInt("losses_quantity"))
            .build();


    @Override
    public void save(Player entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            preparedStatement.setString(1, entity.getPlayerName());
            preparedStatement.setString(2, entity.getPlayerIP());
            preparedStatement.setInt(3, entity.getPlayerScore());
            preparedStatement.setInt(4, entity.getPlayerWins());
            preparedStatement.setInt(5, entity.getPlayerLosses());

            int insertRowsCount = preparedStatement.executeUpdate();
            if (insertRowsCount == 0) {
                System.out.println("Cannot save entity!");
            }

            ResultSet generatedIds = preparedStatement.getGeneratedKeys();
            if (generatedIds.next()) {
                long generatedId = generatedIds.getInt("player_id");
                entity.setPlayerID(generatedId);
//                PreparedStatement preparedStatement1 = connection.prepareStatement(SQL_INSERT_IN_PLAYERS_OF_EACH_GAME);
//                preparedStatement1.setInt(1, generatedId);
//                if (Id1 == 0){
//                    Id1 = generatedId;
//                } else {
//                    Id2 = generatedId;
//                    PreparedStatement preparedStatement2 = connection.prepareStatement(SQL_INSERT_IN_PLAYERS_OF_EACH_GAME);
//                    preparedStatement2.setInt(1, Id1);
//                    preparedStatement2.setInt(2, Id2);
//                    Id1 = 0;
//                    Id2 = 0;
//                }
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();

        } catch (SQLException throwables) {
            throw new IllegalStateException(throwables);
        } finally {
            System.out.println("hello");
        }
    }


    @Override
    public void update(Player entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, entity.getPlayerName());
            statement.setString(2, entity.getPlayerIP());
            statement.setInt(3, entity.getPlayerScore());
            statement.setInt(4, entity.getPlayerWins());
            statement.setInt(5, entity.getPlayerLosses());
            statement.setLong(6, entity.getPlayerID());

            int updateRowsCont = statement.executeUpdate();
            if (updateRowsCont == 0) {
                throw new SQLException("Cannot update player");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }

    @Override
    public Player findByID(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return Player.builder()
                        .playerID(resultSet.getLong("player_id"))
                        .playerName(resultSet.getString("player_name"))
                        .playerIP(resultSet.getString("player_ip"))
                        .playerScore(resultSet.getInt("max_score"))
                        .playerWins(resultSet.getInt("wins_quantity"))
                        .playerLosses(resultSet.getInt("losses_quantity")).build();

            }

            resultSet.close();
            return null;

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Player findByNickName(String nickName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {
            preparedStatement.setString(1, nickName);
            ResultSet row = preparedStatement.executeQuery();
            if (row.next()) {
                return Player.builder()
                        .playerID(row.getLong("player_id"))
                        .playerName(row.getString("player_Name"))
                        .playerIP(row.getString("player_ip"))
                        .playerScore(row.getInt("max_score"))
                        .playerWins(row.getInt("wins_quantity"))
                        .playerLosses(row.getInt("losses_quantity")).build();
            }

            return null;


        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void updateID(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("update player set player_ip = ?," +
                     " max_score = ?, wins_quantity = ?," +
                     " losses_quantity = ?  where player_Name = ?")) {
            preparedStatement.setString(1, player.getPlayerIP());
            preparedStatement.setInt(2, player.getPlayerScore());
            preparedStatement.setInt(3, player.getPlayerWins());
            preparedStatement.setInt(4, player.getPlayerLosses());
            preparedStatement.setString(5, player.getPlayerName());
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }
}
