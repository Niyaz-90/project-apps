package inno.repositories;

import inno.models.Shot;

public interface ShotRepository extends CRUDRepository<Shot, Long> {
    @Override
    void save(Shot entity);
}
