package inno.models;

import lombok.*;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder

public class Game {
    private Long ID;
    private String date ;
    private Player firstPlayer;
    private Long firstPlayerId;
    private Player secondPlayer;
    private Long  secondPlayerId;
    //in seconds
    private String duration;
    private Integer firstPlayerShotsCount ;
    private Integer secondPlayerShotsCount ;



    public String getDate() {
        date = LocalDateTime.now().toString();
        return date;
    }
    public Player getFirstPlayerByID(){
        return firstPlayer;
    }

    public Player getSecondPlayerByID(){
        return secondPlayer;
    }

    public void setFirstPlayer(Player firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public void setSecondPlayer(Player secondPlayer) {
        this.secondPlayer = secondPlayer;
    }
}
