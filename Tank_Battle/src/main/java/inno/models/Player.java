package inno.models;

import lombok.*;

import java.net.Inet4Address;
import java.net.UnknownHostException;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@ToString
public class Player {
    private Long playerID;
    private String playerName;
    private String playerIP;
    private Integer playerScore;
    private Integer playerWins;
    private Integer playerLosses;
    private Integer HP;

    public Player(String playerName) {
        this.playerName = playerName;
        this.playerScore = 0;
        this.playerWins = 0;
        this.playerLosses= 0;
        this.HP = 10;
    }

    public String getPlayerIP() {
        try {
            playerIP = Inet4Address.getLocalHost().getHostAddress();

        } catch (UnknownHostException e) {
            IllegalStateException pp;
        }
        return playerIP;
    }
}
