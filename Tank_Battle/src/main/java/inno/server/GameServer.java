package inno.server;

import inno.service.TanksService;
import inno.service.TanksServiceImpl;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class GameServer {
    private Socket firstPlayer;
    private Socket secondPlayer;

    private Long gameID;
    private PlayerThread firstPlayerThread;
    private PlayerThread secondPlayerThread;


    private TanksService tanksService = new TanksServiceImpl();
    private boolean isGameStarted = false;

    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (!isGameStarted) {
                if (firstPlayer == null) {
                    firstPlayer = serverSocket.accept();
                    System.out.println("Первый игрок подключен");
                    firstPlayerThread = new PlayerThread(firstPlayer);
                    firstPlayerThread.playerValue = "PLAYER_1";
                    firstPlayerThread.start();
                } else {
                    secondPlayer = serverSocket.accept();
                    System.out.println("Второй игрок подключен");
                    secondPlayerThread = new PlayerThread(secondPlayer);
                    secondPlayerThread.playerValue = "PLAYER_2";
//                    secondPlayerThread.nickname = secondPlayer.
                    secondPlayerThread.start();
                    isGameStarted = true;
                }

            }

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private class PlayerThread extends Thread {
        private Socket player;
        private String nickname;
        private String playerValue;
        private PrintWriter toClient;
        private BufferedReader fromClient;
        private int firstPlayerTotalScore = 0;
        private int secondPlayerTotalScore = 0;


        public PlayerThread(Socket player) {
            this.player = player;
            try {
                this.toClient = new PrintWriter(player.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(player.getInputStream()));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        @Override
        public void run() {
            while (true) {
                try {

                    String messageFromClient = fromClient.readLine();
                    if (messageFromClient != null) {
                        if (firstPlayerThread.isAlive() && secondPlayerThread.isAlive()){
                            firstPlayerThread.toClient.println(messageFromClient);
                            secondPlayerThread.toClient.println(messageFromClient);
                        } else {
                            break;
                        }
                        System.out.println("Получили сообщение от " + player.getInetAddress().getHostAddress() + " - " + messageFromClient);
                        String[] command = messageFromClient.split(" ");
                         if (command[0].equals("start")) {

                            this.nickname = command[1];
                            if (firstPlayerThread.nickname != null & secondPlayerThread.nickname != null) {
                                gameID = tanksService.startGame(firstPlayerThread.nickname, secondPlayerThread.nickname);
                                firstPlayerThread.toClient.println("PLAYER_1");
                                secondPlayerThread.toClient.println("PLAYER_2");
                            }
                        } else if (command[0].equals("shot")){
                                if (command[1].equals("PLAYER_1")) {
                                    tanksService.shot(gameID, firstPlayerThread.nickname, secondPlayerThread.nickname);
                                } else if (command[1].equals("PLAYER_2")) {
                                    tanksService.shot(gameID, secondPlayerThread.nickname, firstPlayerThread.nickname);
                                }
                            }

                            else if (command[0].equals("hit")) {
                                if (command[1].equals("PLAYER_1")) {
                                    this.secondPlayerTotalScore += 1;
                                } else if (command[1].equals("PLAYER_2")) {
                                    this.firstPlayerTotalScore += 1;
                                }
                            } else if (command[0].equals("gameOver")) {
                                if (command[1].equals("PLAYER_1")) {
                                    tanksService.endGame(gameID, secondPlayerThread.nickname, secondPlayerTotalScore, firstPlayerThread.nickname, firstPlayerTotalScore);

                                } else {
                                    tanksService.endGame(gameID, firstPlayerThread.nickname, firstPlayerTotalScore, secondPlayerThread.nickname, secondPlayerTotalScore);
                                }


                                firstPlayerThread.toClient.close();
                                secondPlayerThread.fromClient.close();
                                firstPlayer.close();
                                secondPlayer.close();
                            }




                        }





                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }

            }
        }
    }
}



