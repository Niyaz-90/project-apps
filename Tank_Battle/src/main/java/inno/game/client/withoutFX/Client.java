package inno.game.client.withoutFX;

import inno.models.Player;

import java.io.*;
import java.net.Socket;

public class Client {
    private Socket client;
    private PrintWriter toServer;
    private BufferedReader fromServer;
    private Integer health = 5;
    private Player player;

    public Client( Player player) {
        this.player = player;

        try {
            this.client = new Socket("localhost", 7777);
            this.toServer = new PrintWriter(client.getOutputStream(), true);
            this.fromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendMessage(String message){
        toServer.println(message);
    }

    public Runnable receiverMessagesTask = () -> {
        while (health != 0){
            sendMessage("hit " + player.getPlayerName());
            this.health -= 1;
        }

        sendMessage("gameOver " + player.getPlayerName());
    };
}
