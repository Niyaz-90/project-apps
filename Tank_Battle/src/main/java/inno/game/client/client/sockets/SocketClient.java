package inno.game.client.client.sockets;

import inno.game.client.client.controller.Controller;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient {

    private Controller controller;

    private Socket client;

    private PrintWriter toServer; // поток для сервера
    private BufferedReader fromServer; // поток с сервера

    public SocketClient(String host, int port, Controller controller) {
        try {
            this.controller = controller;
            client = new Socket(host, port); // подключаемся к серверу
            toServer = new PrintWriter(client.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(receiverMessagesTask).start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void sendMessage(String message) {
        toServer.println(message);
    }

    private Runnable receiverMessagesTask = () -> {
        while (true) {
            try {
                String messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    if (messageFromServer.startsWith("PLAYER")) {
                        controller.setPlayerNumber(messageFromServer);
                    }

                    if (messageFromServer.contains("move")) {
                        String[] parsedMessage = messageFromServer.split(" ");
                        if (!parsedMessage[2].equals(controller.getPlayerNumber())) {
                            if (parsedMessage[1].equals("RIGHT")) {
                                controller.getEnemy().setCenterX(controller.getEnemy().getCenterX() + 5);
                            } else {
                                controller.getEnemy().setCenterX(controller.getEnemy().getCenterX() - 5);
                            }
                        }
                    }
                    if (messageFromServer.contains("shot")) {
                        String[] parsedMessage = messageFromServer.split(" ");
                        if (!parsedMessage[1].equals(controller.getPlayerNumber())) {
                            Platform.runLater(() -> {
                                Circle bullet = new Circle();
                                bullet.setRadius(5);
                                bullet.setCenterX(controller.getEnemy().getCenterX() + controller.getEnemy().getLayoutX());
                                bullet.setCenterY(controller.getEnemy().getCenterY() + controller.getEnemy().getLayoutY());
                                bullet.setFill(Color.ORANGE);
                                controller.getPane().getChildren().add(bullet);

                                Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
                                    bullet.setCenterY(bullet.getCenterY() + 1);

                                    if (bullet.getBoundsInParent().intersects(controller.getPlayer().getBoundsInParent())) {
                                        bullet.setVisible(false);
                                        controller.getPane().getChildren().remove(bullet);
                                    }

                                }));

                                timeline.setCycleCount(500);
                                timeline.play();
                            });
                        }
                    }
                    if (messageFromServer.contains("gameOver")) {
                        String[] parsedMessage = messageFromServer.split(" ");
                        String player = parsedMessage[1];

                        sendMessage("gameOver " + player);

                        fromServer.close();
                        toServer.close();
                        client.close();
                    }
                    if (messageFromServer.contains("hit")) {
                        String[] parsedMessage = messageFromServer.split(" ");
                        String player = parsedMessage[1];
                        if (controller.getPlayerNumber().equals(player)) {
                            Platform.runLater(() ->
                                    controller.getPlayerHP().setText(String.valueOf(Integer.parseInt(controller.getPlayerHP().getText()) - 50))
                            );
                        } else {
                            Platform.runLater(() ->
                                    controller.getEnemyHP().setText(String.valueOf(Integer.parseInt(controller.getEnemyHP().getText()) - 50)));
                        }
                    }

                    controller.getServerLogsTextArea().appendText(messageFromServer + "\n");
                }

            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    };
}