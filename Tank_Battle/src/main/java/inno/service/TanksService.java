package inno.service;

public interface TanksService {
    Long startGame(String firstPlayerName, String secondPlayerName);
    void shot(Long gameID, String shooter, String target);
    void endGame(Long game, String winnerName, int winnerScore, String loserName, int loserScore);
//    void updatePlayerScore()

}
