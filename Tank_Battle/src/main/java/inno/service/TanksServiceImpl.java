package inno.service;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import inno.models.Game;
import inno.models.Player;
import inno.models.Shot;
import inno.repositories.*;

import java.time.Duration;
import java.time.LocalDateTime;

public class TanksServiceImpl implements TanksService {

    HikariConfig hikariConfig ;
    HikariDataSource dataSource ;

    PlayerRepository playerRepository;
    GameRepository gameRepository;
    ShotRepository shotRepository;

    public TanksServiceImpl() {
        this.hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/project_DB");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("qwerty123");

        this.dataSource = new HikariDataSource(hikariConfig);

        this.playerRepository = new PlayerRepositoryImpl(dataSource);
        this.gameRepository = new GameRepositoryImpl(dataSource);
        this.shotRepository = new ShotRepositoryImpl(dataSource);
    }



    @Override
    public Long startGame(String firstPlayerName, String secondPlayerName) {

        Player firstPlayer = playerRepository.findByNickName(firstPlayerName);
        Player secondPlayer = playerRepository.findByNickName(secondPlayerName);

        if (firstPlayer == null){
            firstPlayer = new Player(firstPlayerName);
            playerRepository.save(firstPlayer);
        } else {
          playerRepository.updateID(firstPlayer);
        }

        if (secondPlayer == null){
            secondPlayer = new Player(secondPlayerName);
            playerRepository.save(secondPlayer);
        } else {
            playerRepository.updateID(secondPlayer);
        }

        Game game = new Game();
        game.setFirstPlayer(firstPlayer);
        game.setSecondPlayer(secondPlayer);
        game.getDate();
        gameRepository.save(game);
        gameRepository.addPlayersToGame(game.getID(), firstPlayer, secondPlayer);
        return game.getID();
    }

    @Override
    public void shot(Long gameID, String shooterName, String targetName) {

        Player shooter = playerRepository.findByNickName(shooterName);
        Player target = playerRepository.findByNickName(targetName);
        Game game = gameRepository.findByID(gameID);
        game.setFirstPlayer(playerRepository.findByID(game.getFirstPlayerId()));
        game.setSecondPlayer(playerRepository.findByID(game.getSecondPlayerId()));


        Shot shot = new Shot(game, shooter, target);
        if (game.getFirstPlayer().equals(shooter)){
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        } else if (game.getSecondPlayer().equals(shooter)){
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }

        shotRepository.save(shot);
        gameRepository.update(game);
        System.out.println(" shots: " + game.getFirstPlayerShotsCount() + "|" + game.getSecondPlayerShotsCount());

    }


    @Override
    public void endGame(Long gameId, String winnerName, int winnerScore, String loserName, int loserScore){
        Game game = gameRepository.findByID(gameId);
        Player winner = playerRepository.findByNickName(winnerName);
        if (winner == null){
            System.out.println("Winner not found");
        }
        Player loser = playerRepository.findByNickName(loserName);
        if (loser == null){
            System.out.println("Loser not found");
        }
       Duration duration = Duration.between(LocalDateTime.parse(game.getDate()), LocalDateTime.now());
        game.setDuration(duration.toString());
        gameRepository.update(game);

        assert winner != null;
        if (winner.getPlayerName().equals(game.getFirstPlayer().getPlayerName())){
            winner.setPlayerScore(winnerScore);
            loser.setPlayerScore(loserScore);
        } else {
            winner.setPlayerScore(game.getSecondPlayerShotsCount());
            loser.setPlayerScore(game.getFirstPlayerShotsCount());
        }
        winner.setPlayerWins(winner.getPlayerWins() + 1);
        loser.setPlayerLosses(loser.getPlayerLosses() + 1);

        playerRepository.update(winner);
        playerRepository.update(loser);
    }



//    public void
}


