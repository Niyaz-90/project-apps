package program.server;

import program.service.Service;
import program.service.ServiceImpl;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static ExecutorService multiServer = Executors.newFixedThreadPool(4);

    private static final String FUTURES = "Euro FX Futures, Japanese Yen Futures, British Pound Futures," +
            " Canadian Dollar Futures, Australian Dollar Futures" ;

    private Socket client;
    private ClientThread userThread;
    private final Service service;


    public Server() {
        this.service = new ServiceImpl();
    }

    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            if (client == null) {

                client = serverSocket.accept();
                userThread = new ClientThread(client);
                userThread.toUser.println("Вы подключились к серверу");
                System.out.println(client.getInetAddress().getHostAddress() + " " + client.getInetAddress().getHostName());
                service.downloadFuturesInfo();
                userThread.start();

            } else {
                multiServer.execute(userThread);
//                System.out.println("Превышено количество допустимых пользователей");
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public class ClientThread extends Thread {
        private Socket user;
        private PrintWriter toUser;
        private BufferedReader fromUser;

        public ClientThread(Socket user) {
            this.user = user;
            try {
                this.toUser = new PrintWriter(user.getOutputStream(), true);
                this.fromUser = new BufferedReader(new InputStreamReader(user.getInputStream()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }


        @Override
        public void run() {

            while (true){

                try {
                    String userMessage = fromUser.readLine();


                    if (userMessage.equals("go")){

                        String[] splitted = FUTURES.split(", ");
                            service.writeToXL(splitted);

                        toUser.println("That's all");
                        toUser.println("exit");
                        toUser.close();
                        fromUser.close();
                        userThread.user.close();
                        user.close();
                        multiServer.shutdown();
                        System.exit(0);


                    } else {
                        System.out.println("exit");
                        System.exit(-1);
                    }
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }


        }
    }
}
