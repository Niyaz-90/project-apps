package program.service;

import program.data.*;

public class ServiceImpl implements Service {
    private DataGetter dataGetter;
    private DataProcessor dataProcessor;
    private DataSetter dataSetter;

    public ServiceImpl() {
        this.dataGetter = new DataGetterImpl();
        this.dataSetter = new DataSetterImpl();
    }

    @Override
    public void downloadFuturesInfo() {
        dataGetter.getJsonTable();

    }

    @Override
    public void writeToXL(String[] futuresArray) {
        for (String future : futuresArray) {
            dataProcessor = new DataProcessorImpl(future);
            dataSetter.putOptionInfo(dataProcessor.structuringOptionData());
            dataSetter.putForwardPoints(dataProcessor.forwardPointProcessor());

        }

    }
}
