package program.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
    private Socket user;
    private PrintWriter toServer;
    private BufferedReader fromServer;

    public Client() {

        try {
            user = new Socket("localhost", 8573);
            toServer = new PrintWriter(user.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(user.getInputStream()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public void sendMessage(String message) {
        toServer.println(message);
    }

    public Runnable receiverMessagesTask = () -> {
        toServer.println("go");
      while (true){
          try {
              String messagefromServer = fromServer.readLine();
              System.out.println(messagefromServer);

              if (messagefromServer.equals("exit")){
                  fromServer.close();
                  toServer.close();
                  user.close();
                  break;
              }
          } catch (IOException e) {
              throw new IllegalArgumentException(e);
          }
      }
    };


}
