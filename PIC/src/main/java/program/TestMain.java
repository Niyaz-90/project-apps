package program;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class TestMain {
    public static void main(String[] args) {
        try {
            Document investing = Jsoup.connect("https://ru.investing.com/currencies/").get();
            Elements priceTable = investing.select("table#cr1.genTbl.closedTbl.crossRatesTbl");
            int i = 1;
            while (i <= 4){
                System.out.println(priceTable.select("tr#pair_" + i).select("td").get(3).text());
                i++;
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
