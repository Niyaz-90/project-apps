package program.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class ForwardPoint {
    private String CurrencyName;
    private double openPriceFuture;
    private double openPriceSpot;
    private double FP;

}
