package program.data;

import program.models.ForwardPoint;
import program.models.CurrencyOption;

public interface DataProcessor {
    ForwardPoint forwardPointProcessor();
    CurrencyOption structuringOptionData();
    Double getFutureOpenPrice();
    Double getSpotOpenPrice();
}
