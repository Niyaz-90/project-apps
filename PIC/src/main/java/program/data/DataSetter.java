package program.data;

import program.models.ForwardPoint;
import program.models.CurrencyOption;

public interface DataSetter {
    void putOptionInfo(CurrencyOption option);
    void putForwardPoints(ForwardPoint forwardPoint);

}
