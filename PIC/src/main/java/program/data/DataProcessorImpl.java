package program.data;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import program.models.ForwardPoint;
import program.models.CurrencyOption;

import java.io.*;
import java.time.LocalDate;

public class DataProcessorImpl implements DataProcessor {
    private static final String JSON_FILE = "C:/Users/niyaz/Git/json's/CME_tempData_" + LocalDate.now() + "_.json";
   private static final String HTML_FILE_PROCESSED_DATA = "C:/Users/niyaz/Git/json's/QuikStrike_tempData_" + LocalDate.now() + "_.csv";
    private ForwardPoint currencyObj;
    private CurrencyOption currencyOption;
    private File fileForJsoup;
    private String currency;

    public DataProcessorImpl(String currency) {
        this.currency = currency;
    }

    @Override
    public ForwardPoint forwardPointProcessor() {

        currencyObj = new ForwardPoint();
        currencyObj.setCurrencyName(this.currency);
        currencyObj.setOpenPriceFuture(getFutureOpenPrice());
        currencyObj.setOpenPriceSpot(getSpotOpenPrice());

        currencyObj.setFP(currencyObj.getOpenPriceSpot() - currencyObj.getOpenPriceFuture());


        return currencyObj;


    }

    @Override
    public CurrencyOption structuringOptionData() {
//        currencyOption = new CurrencyOption();

        fileForJsoup = new File("C:/Users/niyaz/Git/json's/html's/" + this.currency + ".html");
//        currencyOption.setOptionName(this.currency + "_Future");
        try {
            Document html = Jsoup.parse(fileForJsoup, "UTF-8");

            FileWriter htmlFile = new FileWriter(HTML_FILE_PROCESSED_DATA, true);
            Elements trATM = html.select("table#pricing-sheet.grid-thm.base-sheet.w-lg.grid-thm-v2").select("tr.currentATM");
            htmlFile.write(this.currency + trATM.text());

            String[] fromHtml = trATM.text().split(" ");

            for (int i = 0; i < fromHtml.length; i++) {
                fromHtml[i] = fromHtml[i].replace(",", ".");
            }


            htmlFile.close();
            return CurrencyOption.builder()
                    .optionName(this.currency)
                    .volatility(Double.parseDouble(fromHtml[0]))
                    .call_delta(Double.parseDouble(fromHtml[1]))
                    .call_settle(Double.parseDouble(fromHtml[2]))
                    .call(Double.parseDouble(fromHtml[3]))
                    .strike(Double.parseDouble(fromHtml[4]))
                    .put(Double.parseDouble(fromHtml[5]))
                    .put_settle(Double.parseDouble(fromHtml[6]))
                    .put_delta(Double.parseDouble(fromHtml[7]))
                    .straddle(Double.parseDouble(fromHtml[8]))
                    .gamma(Double.parseDouble(fromHtml[9]))
                    .vega(Double.parseDouble(fromHtml[10]))
                    .theta(Double.parseDouble(fromHtml[11]))
                    .build();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Double getFutureOpenPrice() {
        Double futurePrice = null;
        try {
            Reader reader = new FileReader(JSON_FILE);


            JSONParser json = new JSONParser();
            JSONArray obj = (JSONArray) json.parse(reader);
            JSONObject currencyObject = (JSONObject) obj.get(0);
            int i = 0;


            while (!currencyObject.get("productName").equals(this.currency) & i < obj.size()) {

                currencyObject = (JSONObject) obj.get(i);
                i++;
            }
            reader.close();
            futurePrice = Double.parseDouble(currencyObject.get("open").toString());

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return futurePrice;
    }

    @Override
    public Double getSpotOpenPrice() {
        Double spotPrice = null;
        try {
            Document investing = Jsoup.connect("https://ru.investing.com/currencies/").get();

            Elements priceTable = investing.select("table#cr1.genTbl.closedTbl.crossRatesTbl");
//            priceTable.text().replace(",", ".");


            switch (this.currency) {
                case "Euro FX Futures":
                    spotPrice = Double.parseDouble(priceTable.select("tr#pair_1").select("td").get(3).text().replace(",", "."));
                    break;
                case "British Pound Futures":
                    spotPrice = Double.parseDouble(priceTable.select("tr#pair_2").select("td").get(3).text().replace(",", "."));
                    break;
                case "Japanese Yen Futures":
                    spotPrice = Double.parseDouble(priceTable.select("tr#pair_3").select("td").get(3).text().replace(",", "."));
                    break;
                case "Australian Dollar Futures":
                    spotPrice = Double.parseDouble(priceTable.select("tr#pair_5").select("td").get(3).text().replace(",", "."));
                    break;
                case "Canadian Dollar Futures":
                    spotPrice = Double.parseDouble(priceTable.select("tr#pair_7").select("td").get(3).text().replace(",", "."));
                    break;

            }

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return spotPrice;
    }

}
