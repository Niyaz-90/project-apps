package program.data;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import program.models.ForwardPoint;
import program.models.CurrencyOption;

import java.io.*;


public class DataSetterImpl implements DataSetter {
    private final FileInputStream file;
    private final Workbook exsel;

    public DataSetterImpl() {
        try {
            file = new FileInputStream("C:/Users/niyaz/Git/xlxs/source_File.xlsx");
            exsel = new XSSFWorkbook(file);
            file.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void putOptionInfo(CurrencyOption option) {

        try {

            Sheet fromYesterdayToTomorrow = exsel.getSheetAt(0);

            if (!fromYesterdayToTomorrow.getSheetName().equals("Данные вчера перевод в завтра")) {
                System.out.println("неверное название листа");
                System.exit(-1);
            }

            switch (option.getOptionName()) {
                case "Euro FX Futures":
                    Row rowEUR = fromYesterdayToTomorrow.getRow(1);
                    if (!rowEUR.getCell(0).toString().equals("EUR")) {
                        System.out.println("not EUR");
                        System.exit(-1);
                    }

                    Cell b2 = rowEUR.getCell(1);
                    b2.setCellValue(option.getVolatility());
                    Cell c2 = rowEUR.getCell(2);
                    c2.setCellValue(option.getCall_delta());
                    Cell d2 = rowEUR.getCell(3);
                    d2.setCellValue(option.getCall_settle());
                    Cell e2 = rowEUR.getCell(4);
                    e2.setCellValue(option.getCall());
                    Cell f2 = rowEUR.getCell(5);
                    f2.setCellValue(option.getStrike());
                    Cell g2 = rowEUR.getCell(6);
                    g2.setCellValue(option.getPut());
                    Cell h2 = rowEUR.getCell(7);
                    h2.setCellValue(option.getPut_settle());
                    Cell i2 = rowEUR.getCell(8);
                    i2.setCellValue(option.getPut_delta());
                    Cell j2 = rowEUR.getCell(9);
                    j2.setCellValue(option.getStraddle());
                    Cell k2 = rowEUR.getCell(10);
                    k2.setCellValue(option.getGamma());
                    Cell l2 = rowEUR.getCell(11);
                    l2.setCellValue(option.getVega());
                    Cell m2 = rowEUR.getCell(12);
                    m2.setCellValue(option.getTheta());
                    System.out.println("EUR is modified");
                    break;


                case "British Pound Futures":
                    Row rowGBP = fromYesterdayToTomorrow.getRow(15);
                    if (!rowGBP.getCell(0).toString().equals("GBP")) {
                        System.out.println("not GBP");
                        break;
                    }

                    Cell b15 = rowGBP.getCell(1);
                    b15.setCellValue(option.getVolatility());
                    Cell c15 = rowGBP.getCell(2);
                    c15.setCellValue(option.getCall_delta());
                    Cell d15 = rowGBP.getCell(3);
                    d15.setCellValue(option.getCall_settle());
                    Cell e15 = rowGBP.getCell(4);
                    e15.setCellValue(option.getCall());
                    Cell f15 = rowGBP.getCell(5);
                    f15.setCellValue(option.getStrike());
                    Cell g15 = rowGBP.getCell(6);
                    g15.setCellValue(option.getPut());
                    Cell h15 = rowGBP.getCell(7);
                    h15.setCellValue(option.getPut_settle());
                    Cell i15 = rowGBP.getCell(8);
                    i15.setCellValue(option.getPut_delta());
                    Cell j15 = rowGBP.getCell(9);
                    j15.setCellValue(option.getStraddle());
                    Cell k15 = rowGBP.getCell(10);
                    k15.setCellValue(option.getGamma());
                    Cell l15 = rowGBP.getCell(11);
                    l15.setCellValue(option.getVega());
                    Cell m15 = rowGBP.getCell(12);
                    m15.setCellValue(option.getTheta());
                    System.out.println("GBP is modified");
                    break;

                case "Canadian Dollar Futures":
                    Row rowCAD = fromYesterdayToTomorrow.getRow(22);
                    if (!rowCAD.getCell(0).toString().equals("CAD")) {
                        System.out.println("not CAD");
                        System.exit(-1);
                    }

                    Cell b22 = rowCAD.getCell(1);
                    b22.setCellValue(option.getVolatility());
                    Cell c22 = rowCAD.getCell(2);
                    c22.setCellValue(option.getCall_delta());
                    Cell d22 = rowCAD.getCell(3);
                    d22.setCellValue(option.getCall_settle());
                    Cell e22 = rowCAD.getCell(4);
                    e22.setCellValue(option.getCall());
                    Cell f22 = rowCAD.getCell(5);
                    f22.setCellValue(option.getStrike());
                    Cell g22 = rowCAD.getCell(6);
                    g22.setCellValue(option.getPut());
                    Cell h22 = rowCAD.getCell(7);
                    h22.setCellValue(option.getPut_settle());
                    Cell i22 = rowCAD.getCell(8);
                    i22.setCellValue(option.getPut_delta());
                    Cell j22 = rowCAD.getCell(9);
                    j22.setCellValue(option.getStraddle());
                    Cell k22 = rowCAD.getCell(10);
                    k22.setCellValue(option.getGamma());
                    Cell l22 = rowCAD.getCell(11);
                    l22.setCellValue(option.getVega());
                    Cell m22 = rowCAD.getCell(12);
                    m22.setCellValue(option.getTheta());
                    System.out.println("CAD is modified");
                    break;

                case "Japanese Yen Futures" :
                    Row rowJPY = fromYesterdayToTomorrow.getRow(29);
                    if (!rowJPY.getCell(0).toString().equals("JPY")) {
                        System.out.println("not JPY");
                        System.exit(-1);
                    }

                    Cell b29 = rowJPY.getCell(1);
                    b29.setCellValue(option.getVolatility());
                    Cell c29 = rowJPY.getCell(2);
                    c29.setCellValue(option.getCall_delta());
                    Cell d29 = rowJPY.getCell(3);
                    d29.setCellValue(option.getCall_settle());
                    Cell e29 = rowJPY.getCell(4);
                    e29.setCellValue(option.getCall());
                    Cell f29 = rowJPY.getCell(5);
                    f29.setCellValue(option.getStrike());
                    Cell g29 = rowJPY.getCell(6);
                    g29.setCellValue(option.getPut());
                    Cell h29 = rowJPY.getCell(7);
                    h29.setCellValue(option.getPut_settle());
                    Cell i29 = rowJPY.getCell(8);
                    i29.setCellValue(option.getPut_delta());
                    Cell j29 = rowJPY.getCell(9);
                    j29.setCellValue(option.getStraddle());
                    Cell k29 = rowJPY.getCell(10);
                    k29.setCellValue(option.getGamma());
                    Cell l29 = rowJPY.getCell(11);
                    l29.setCellValue(option.getVega());
                    Cell m29 = rowJPY.getCell(12);
                    m29.setCellValue(option.getTheta());
                    System.out.println("JPY is modified");
                    break;

                case "Australian Dollar Futures":
                    Row rowAUD = fromYesterdayToTomorrow.getRow(36);
                    if (!rowAUD.getCell(0).toString().equals("AUD")) {
                        System.out.println("not AUD");
                        System.exit(-1);
                    }

                    Cell b36 = rowAUD.getCell(1);
                    b36.setCellValue(option.getVolatility());
                    Cell c36 = rowAUD.getCell(2);
                    c36.setCellValue(option.getCall_delta());
                    Cell d36 = rowAUD.getCell(3);
                    d36.setCellValue(option.getCall_settle());
                    Cell e36 = rowAUD.getCell(4);
                    e36.setCellValue(option.getCall());
                    Cell f36 = rowAUD.getCell(5);
                    f36.setCellValue(option.getStrike());
                    Cell g36 = rowAUD.getCell(6);
                    g36.setCellValue(option.getPut());
                    Cell h36 = rowAUD.getCell(7);
                    h36.setCellValue(option.getPut_settle());
                    Cell i36 = rowAUD.getCell(8);
                    i36.setCellValue(option.getPut_delta());
                    Cell j36 = rowAUD.getCell(9);
                    j36.setCellValue(option.getStraddle());
                    Cell k36 = rowAUD.getCell(10);
                    k36.setCellValue(option.getGamma());
                    Cell l36 = rowAUD.getCell(11);
                    l36.setCellValue(option.getVega());
                    Cell m36 = rowAUD.getCell(12);
                    m36.setCellValue(option.getTheta());
                    System.out.println("AUD is modified");
                    break;



            }
            FileOutputStream saveXLSX = new FileOutputStream("C:/Users/niyaz/Git/xlxs/source_File.xlsx");
            exsel.write(saveXLSX);
            saveXLSX.close();


        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void putForwardPoints(ForwardPoint forwardPoint){
        Sheet fromYesterdayToTomorrow = exsel.getSheetAt(0);

        if (!fromYesterdayToTomorrow.getSheetName().equals("Данные вчера перевод в завтра")) {
            System.out.println("неверное название листа");
            System.exit(-1);
        }
        switch (forwardPoint.getCurrencyName()){
            case "Australian Dollar Futures":
                Cell c58 = fromYesterdayToTomorrow.getRow(57).getCell(2);
                c58.setCellValue(forwardPoint.getFP());
                break;
            case "Canadian Dollar Futures":
                Cell c59 = fromYesterdayToTomorrow.getRow(58).getCell(2);
                c59.setCellValue(forwardPoint.getFP());
                break;
            case "Euro FX Futures":
                Cell c61 = fromYesterdayToTomorrow.getRow(60).getCell(2);
                c61.setCellValue(forwardPoint.getFP());
                break;
            case "British Pound Futures":
                Cell c62 = fromYesterdayToTomorrow.getRow(61).getCell(2);
                c62.setCellValue(forwardPoint.getFP());
                break;
            case "Japanese Yen Futures":
                Cell c63 = fromYesterdayToTomorrow.getRow(62).getCell(2);
                c63.setCellValue(forwardPoint.getFP());
                break;
        }

        try {
            FileOutputStream saveXLSX = new FileOutputStream("C:/Users/niyaz/Git/xlxs/source_File.xlsx");
            exsel.write(saveXLSX);
            saveXLSX.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }
}
