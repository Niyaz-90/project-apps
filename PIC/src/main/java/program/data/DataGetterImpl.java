package program.data;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.time.LocalDate;

public class DataGetterImpl implements DataGetter {
    private static final String FUTURES = "Euro FX Futures, Japanese Yen Futures, British Pound Futures," +
            " Canadian Dollar Futures, Australian Dollar Futures," +
            "Swiss Franc Futures";
    public void getJsonTable() {
        try {
            URL webJSON = new URL("https://www.cmegroup.com/CmeWS/mvc/Quotes/FrontMonths?productIds=58,130,69,131,37,42,48,86,78,826,825&venue=G&type=VOLUME");

            ReadableByteChannel jsonFromWeb = Channels.newChannel(webJSON.openStream());
            FileOutputStream jsonFile = new FileOutputStream("C:/Users/niyaz/Git/json's/CME_tempData_" + LocalDate.now() + "_.json");
            jsonFile.getChannel().transferFrom(jsonFromWeb, 0, Long.MAX_VALUE);

        } catch (MalformedURLException e) {
            throw new IllegalStateException("Cannot find page/file from this URL");
        } catch (IOException e) {
            throw new IllegalArgumentException("Channel is not work");
        }
    }

}
